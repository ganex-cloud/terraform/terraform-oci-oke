# public worker security checklist
resource "oci_core_security_list" "workers_public" {
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-workers-public"
  vcn_id         = var.vcn_id

  dynamic "egress_security_rules" {
    for_each = local.all_subnets

    content {
      description      = "Allow pods on one worker node/subnet to communicate with pods on other worker nodes/subnet"
      protocol         = local.all_protocols
      destination      = egress_security_rules.value
      destination_type = "CIDR_BLOCK"
      stateless        = true
    }
  }

  egress_security_rules {
    description = "Allow all outbound traffic to the internet. Required for getting container images or using external services"
    destination = local.anywhere
    protocol    = local.all_protocols
    stateless   = false
  }

  egress_security_rules {
    description      = "This rule enables outbound ICMP traffic."
    destination      = local.osn
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = tostring(local.icmp_protocol)
    stateless        = false
  }

  egress_security_rules {
    description      = "This rule enables worker nodes to communicate with Container Engine for Kubernetes to ensure correct start-up, and continued functioning."
    destination      = local.osn
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = tostring(local.tcp_protocol)
    stateless        = false

    tcp_options {
      min = 80
      max = 80
      source_port_range {
        max = 80
        min = 80
      }
    }
  }

  egress_security_rules {
    description      = "This rule enables worker nodes to communicate with Container Engine for Kubernetes to ensure correct start-up, and continued functioning."
    destination      = local.osn
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = tostring(local.tcp_protocol)
    stateless        = false

    tcp_options {
      min = 443
      max = 443
      source_port_range {
        max = 443
        min = 443
      }
    }
  }

  egress_security_rules {
    description      = "This rule enables worker nodes to communicate with Container Engine for Kubernetes to ensure correct start-up, and continued functioning."
    destination      = local.osn
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = tostring(local.tcp_protocol)
    stateless        = false

    tcp_options {
      min = 6443
      max = 6443
      source_port_range {
        max = 6443
        min = 6443
      }
    }
  }

  egress_security_rules {
    description      = "This rule enables worker nodes to communicate with Container Engine for Kubernetes to ensure correct start-up, and continued functioning."
    destination      = local.osn
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = tostring(local.tcp_protocol)
    stateless        = false

    tcp_options {
      min = 12250
      max = 12250
      source_port_range {
        max = 12250
        min = 12250
      }
    }
  }

  dynamic "egress_security_rules" {
    for_each = var.worker_public_additional_egress_security_rules
    content {
      description      = lookup(egress_security_rules.value, "description")
      protocol         = lookup(egress_security_rules.value, "protocol")
      destination      = lookup(egress_security_rules.value, "destination")
      destination_type = lookup(egress_security_rules.value, "destination_type")
      stateless        = lookup(egress_security_rules.value, "stateless")
      dynamic "tcp_options" {
        for_each = lookup(egress_security_rules.value, "protocol") == "6" ? list(1) : []
        content {
          max = lookup(egress_security_rules.value, "destination_port_range")
          min = lookup(egress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(egress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(egress_security_rules.value, "source_port_range")
              min = lookup(egress_security_rules.value, "source_port_range")
            }
          }
        }
      }
      dynamic "udp_options" {
        for_each = lookup(egress_security_rules.value, "protocol") == "17" ? list(1) : []
        content {
          max = lookup(egress_security_rules.value, "destination_port_range")
          min = lookup(egress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(egress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(egress_security_rules.value, "source_port_range")
              min = lookup(egress_security_rules.value, "source_port_range")
            }
          }
        }
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = local.all_subnets

    content {
      description = "Allow pods on one worker node/subnet to communicate with pods on other worker nodes/subnet"
      protocol    = local.all_protocols
      source      = ingress_security_rules.value
      stateless   = true
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "130.35.0.0/16"
    stateless   = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "134.70.0.0/17"
    stateless   = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "138.1.0.0/16"
    stateless   = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "140.91.0.0/17"
    stateless   = false
    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "147.154.0.0/16"
    stateless   = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "Allow OKE to access worker nodes"
    protocol    = local.tcp_protocol
    source      = "192.29.0.0/16"
    stateless   = false

    tcp_options {
      min = 22
      max = 22
    }
  }

  ingress_security_rules {
    description = "This rule enables worker nodes to receive Path MTU Discovery fragmentation messages."
    protocol    = local.icmp_protocol
    source      = local.anywhere
    stateless   = false

    icmp_options {
      type = 3
      code = 4
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.allow_node_port_access == true ? list(1) : []

    content {
      description = "This rule enables inbound traffic to the worker nodes on the default NodePort range"
      protocol    = local.tcp_protocol
      source      = local.anywhere
      stateless   = false

      tcp_options {
        max = local.node_port_max
        min = local.node_port_min
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.allow_node_port_access == true ? list(1) : []

    content {
      description = "This rule enables inbound traffic to the worker nodes on the default NodePort range"
      protocol    = local.udp_protocol
      source      = local.anywhere
      stateless   = false

      udp_options {
        max = local.node_port_max
        min = local.node_port_min
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.worker_ssh_access_cidr

    content {
      description = "Allow SSH access to worker nodes"
      protocol    = local.tcp_protocol
      source      = ingress_security_rules.value
      stateless   = false
      tcp_options {
        max = 22
        min = 22
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.worker_public_additional_ingress_security_rules
    content {
      description = lookup(ingress_security_rules.value, "description")
      protocol    = lookup(ingress_security_rules.value, "protocol")
      source      = lookup(ingress_security_rules.value, "source")
      stateless   = lookup(ingress_security_rules.value, "stateless")
      dynamic "tcp_options" {
        for_each = lookup(ingress_security_rules.value, "protocol") == "6" ? list(1) : []
        content {
          max = lookup(ingress_security_rules.value, "destination_port_range")
          min = lookup(ingress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(ingress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(ingress_security_rules.value, "source_port_range")
              min = lookup(ingress_security_rules.value, "source_port_range")
            }
          }
        }
      }
      dynamic "udp_options" {
        for_each = lookup(ingress_security_rules.value, "protocol") == "17" ? list(1) : []
        content {
          max = lookup(ingress_security_rules.value, "destination_port_range")
          min = lookup(ingress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(ingress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(ingress_security_rules.value, "source_port_range")
              min = lookup(ingress_security_rules.value, "source_port_range")
            }
          }
        }
      }
    }
  }

  count = var.visibility_type == "public" || var.visibility_type == "both" ? 1 : 0
}

## private worker security checklist
resource "oci_core_security_list" "workers_private" {
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-workers-private"
  vcn_id         = var.vcn_id

  dynamic "egress_security_rules" {
    for_each = local.all_subnets

    content {
      description      = "Allow pods on one worker node/subnet to communicate with pods on other worker nodes/subnet"
      protocol         = local.all_protocols
      destination      = egress_security_rules.value
      destination_type = "CIDR_BLOCK"
      stateless        = true
    }
  }

  egress_security_rules {
    description = "Allow all outbound traffic to the internet. Required for getting container images or using external services"
    destination = local.anywhere
    protocol    = local.all_protocols
    stateless   = false
  }

  egress_security_rules {
    description      = "Allow stateful egress to oracle services network through the service gateway"
    destination      = lookup(data.oci_core_services.this[0].services[0], "cidr_block")
    destination_type = "SERVICE_CIDR_BLOCK"
    protocol         = local.all_protocols
    stateless        = false
  }

  dynamic "egress_security_rules" {
    for_each = var.worker_private_additional_egress_security_rules
    content {
      description      = lookup(egress_security_rules.value, "description")
      protocol         = lookup(egress_security_rules.value, "protocol")
      destination      = lookup(egress_security_rules.value, "destination")
      destination_type = lookup(egress_security_rules.value, "destination_type")
      stateless        = lookup(egress_security_rules.value, "stateless")
      dynamic "tcp_options" {
        for_each = lookup(egress_security_rules.value, "protocol") == "6" ? list(1) : []
        content {
          max = lookup(egress_security_rules.value, "destination_port_range")
          min = lookup(egress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(egress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(egress_security_rules.value, "source_port_range")
              min = lookup(egress_security_rules.value, "source_port_range")
            }
          }
        }
      }
      dynamic "udp_options" {
        for_each = lookup(egress_security_rules.value, "protocol") == "17" ? list(1) : []
        content {
          max = lookup(egress_security_rules.value, "destination_port_range")
          min = lookup(egress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(egress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(egress_security_rules.value, "source_port_range")
              min = lookup(egress_security_rules.value, "source_port_range")
            }
          }
        }
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = local.all_subnets

    content {
      description = "Allow pods on one worker node/subnet to communicate with pods on other worker nodes/subnet"
      protocol    = local.all_protocols
      source      = ingress_security_rules.value
      stateless   = true
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.allow_node_port_access == true ? list(1) : []

    content {
      description = "This rule enables inbound traffic to the worker nodes on the default NodePort range"
      protocol    = local.tcp_protocol
      source      = local.anywhere
      stateless   = false

      tcp_options {
        max = local.node_port_max
        min = local.node_port_min
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.allow_node_port_access == true ? list(1) : []

    content {
      description = "This rule enables inbound traffic to the worker nodes on the default NodePort range"
      protocol    = local.udp_protocol
      source      = local.anywhere
      stateless   = false

      udp_options {
        max = local.node_port_max
        min = local.node_port_min
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.worker_ssh_access_cidr

    content {
      description = "Allow SSH access to worker nodes"
      protocol    = local.tcp_protocol
      source      = ingress_security_rules.value
      stateless   = false
      tcp_options {
        max = 22
        min = 22
      }
    }
  }

  dynamic "ingress_security_rules" {
    for_each = var.worker_private_additional_ingress_security_rules
    content {
      description = lookup(ingress_security_rules.value, "description")
      protocol    = lookup(ingress_security_rules.value, "protocol")
      source      = lookup(ingress_security_rules.value, "source")
      stateless   = lookup(ingress_security_rules.value, "stateless")
      dynamic "tcp_options" {
        for_each = lookup(ingress_security_rules.value, "protocol") == "6" ? list(1) : []
        content {
          max = lookup(ingress_security_rules.value, "destination_port_range")
          min = lookup(ingress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(ingress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(ingress_security_rules.value, "source_port_range")
              min = lookup(ingress_security_rules.value, "source_port_range")
            }
          }
        }
      }
      dynamic "udp_options" {
        for_each = lookup(ingress_security_rules.value, "protocol") == "17" ? list(1) : []
        content {
          max = lookup(ingress_security_rules.value, "destination_port_range")
          min = lookup(ingress_security_rules.value, "destination_port_range")
          dynamic "source_port_range" {
            for_each = lookup(ingress_security_rules.value, "source_port_range") == "" ? [] : list(1)
            content {
              max = lookup(ingress_security_rules.value, "source_port_range")
              min = lookup(ingress_security_rules.value, "source_port_range")
            }
          }
        }
      }
    }
  }

  count = var.visibility_type == "private" || var.visibility_type == "both" ? 1 : 0
}

# internal load balancer security checklist
resource "oci_core_security_list" "loadbalancer_private" {
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-loadbalancers-private"
  vcn_id         = var.vcn_id

  dynamic "egress_security_rules" {
    for_each = local.all_subnets

    content {
      description      = "This rule enables responses from a web application through the service load balancers."
      protocol         = local.all_protocols
      destination      = egress_security_rules.value
      destination_type = "CIDR_BLOCK"
      stateless        = true
    }
  }

  dynamic "ingress_security_rules" {
    for_each = local.all_subnets

    content {
      description = "This rule enables incoming public traffic to service load balancers."
      protocol    = local.all_protocols
      source      = ingress_security_rules.value
      stateless   = true
    }
  }

  count = var.loadbalancer_mode == "private" ? 1 : 0
}

# public load balancer security checklist
resource "oci_core_security_list" "loadbalancer_public" {
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-loadbalancers-public"
  vcn_id         = var.vcn_id

  dynamic "egress_security_rules" {
    for_each = local.all_subnets

    content {
      description      = "This rule enables responses from a web application through the service load balancers."
      protocol         = local.all_protocols
      destination      = local.anywhere
      destination_type = "CIDR_BLOCK"
      stateless        = true
    }
  }

  dynamic "ingress_security_rules" {
    for_each = local.all_subnets

    content {
      description = "This rule enables incoming public traffic to service load balancers."
      protocol    = local.all_protocols
      source      = local.anywhere
      stateless   = true
    }
  }

  count = ((var.loadbalancer_mode == "public")) ? 1 : 0
}
