variable "compartment_id" {
  description = "(Required) The OCID of the compartment in which to create the cluster."
  type        = string
  default     = ""
}

variable "cluster_name" {
  description = "(Required) (Updatable) The name of the cluster. Avoid entering confidential information."
  type        = string
  default     = ""
}

variable "description" {
  description = "(Required) (Updatable) The description you assign to the compartment during creation. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "kms_key_id" {
  description = "(Optional) The OCID of the KMS key to be used as the master encryption key for Kubernetes secret encryption. When used, kubernetesVersion must be at least v1.13.0."
  type        = string
  default     = null
}

variable "kubernetes_version" {
  description = "(Required) (Updatable) The version of Kubernetes to install into the cluster masters and workers."
  type        = string
  default     = ""
}

variable "kubernetes_dashboard_enabled" {
  description = "(Optional) Whether or not to enable the Kubernetes Dashboard add-on."
  type        = bool
  default     = false
}

variable "tiller_enabled" {
  description = "(Optional) Whether or not to enable the Tiller add-on."
  type        = bool
  default     = false
}

variable "pod_security_policy_enabled" {
  description = "(Optional) (Updatable) Whether or not to enable the Pod Security Policy admission controller."
  type        = bool
  default     = false
}

variable "services_cidr" {
  description = "(Optional) The CIDR range used by exposed Kubernetes services (ClusterIPs). This CIDR should not overlap with the VCN CIDR range."
  type        = string
  default     = null
}

variable "pods_cidr" {
  description = "(Optional) The CIDR range used for IP addresses by the pods. A /16 CIDR is generally sufficient. This CIDR should not overlap with any subnet range in the VCN (it can also be outside the VCN CIDR range)."
  type        = string
  default     = null
}

variable "vcn_id" {
  description = "(Required) The OCID of the virtual cloud network (VCN) in which to create the cluster."
  type        = string
  default     = ""
}

variable "ssh_public_key" {
  description = "(Optional) The SSH public key to add to each node in the node pool."
  type        = string
  default     = null
}

variable "node_pools" {
  # https://www.terraform.io/docs/providers/oci/r/containerengine_node_pool.html
  description = "(Optional) Provides the Node Pool resource in Oracle Cloud Infrastructure Container Engine service."
  type        = any
  default     = []
}

variable "availability_domains" {
  description = "(Required) (Updatable) The availability domain in which to place nodes."
  type        = list(string)
  default     = []
}

variable "visibility_type" {
  description = "(Required) Whether to provision public, private or both subnets."
  type        = string
  default     = "private"
}

variable "loadbalancer_mode" {
  description = "(Optional) The type of load balancer subnets to create."
  type        = string
  default     = null
}

variable "allow_node_port_access" {
  default     = true
  description = "(Optional) Whether to allow access to NodePorts when worker nodes are deployed in public mode."
  type        = bool
}

variable "worker_ssh_access_cidr" {
  description = "(Optional) List of CIDR blocks to access worker nodes SSH."
  type        = list(string)
  default     = []
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "worker_private_subnet" {
  description = "(Optional) Required if visibility_type is Private, the CIDR IP address block of the Subnet."
  type        = list(string)
  default     = []
}

variable "worker_public_subnet" {
  description = "(Optional) Required if visibility_type is Public, the CIDR IP address block of the Subnet."
  type        = list(string)
  default     = []
}

variable "worker_private_additional_route_table_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of private rules used for routing destination IPs to network devices."
  type        = list(map(string))
  default     = []
}

variable "worker_public_additional_route_table_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of public rules used for routing destination IPs to network devices."
  type        = list(map(string))
  default     = []
}

variable "loadbalancer_subnet" {
  description = "(Optional) Required if load_balancer_mode is configured, the CIDR IP address block of the Subnet."
  type        = list(string)
  default     = []
}

variable "worker_private_additional_ingress_security_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of additinal security rules."
  type        = list(map(string))
  default     = []
}

variable "worker_private_additional_egress_security_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of additinal security rules."
  type        = list(map(string))
  default     = []
}

variable "worker_public_additional_ingress_security_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of additinal security rules."
  type        = list(map(string))
  default     = []
}

variable "worker_public_additional_egress_security_rules" {
  # https://www.terraform.io/docs/providers/oci/r/core_route_table.html#route_rules-1
  description = "(Optional) (Updatable) The collection of additinal security rules."
  type        = list(map(string))
  default     = []
}
