locals {
  defaults = {
    node_pools = {
      initial_node_labels = {}
    }
  }

  all_subnets = concat(var.worker_private_subnet, var.worker_public_subnet, var.loadbalancer_subnet)
  anywhere    = "0.0.0.0/0"

  # port numbers
  node_port_min = 30000
  node_port_max = 32767
  ssh_port      = 22

  # protocols
  # # special OCI designation for all protocols
  all_protocols = "all"

  # # IANA protocol numbers
  icmp_protocol = 1
  tcp_protocol  = 6
  udp_protocol  = 17

  # oracle services network
  osn = lookup(data.oci_core_services.this[0].services[0], "cidr_block")
}
