resource "oci_containerengine_cluster" "this" {
  compartment_id     = var.compartment_id
  kubernetes_version = var.kubernetes_version
  kms_key_id         = var.kms_key_id
  name               = var.cluster_name
  vcn_id             = var.vcn_id

  options {
    add_ons {
      is_kubernetes_dashboard_enabled = var.kubernetes_dashboard_enabled
      is_tiller_enabled               = var.tiller_enabled
    }

    kubernetes_network_config {
      pods_cidr     = var.pods_cidr
      services_cidr = var.services_cidr
    }

    service_lb_subnet_ids = var.loadbalancer_mode == "public" ? [oci_core_subnet.loadbalancers_public[0].id] : [oci_core_subnet.loadbalancers_private[0].id]
  }
}

resource "oci_containerengine_node_pool" "this" {
  count              = length(var.node_pools)
  cluster_id         = oci_containerengine_cluster.this.id
  compartment_id     = var.compartment_id
  kubernetes_version = var.kubernetes_version
  name               = lookup(var.node_pools[count.index], "name")

  dynamic "initial_node_labels" {
    for_each = lookup(var.node_pools[count.index], "initial_node_labels", local.defaults.node_pools.initial_node_labels)
    content {
      key   = initial_node_labels.key
      value = initial_node_labels.value
    }
  }

  node_config_details {
    dynamic "placement_configs" {
      iterator = ad_iterator
      for_each = var.availability_domains
      content {
        availability_domain = ad_iterator.value
        subnet_id           = lookup(var.node_pools[count.index], "mode") == "public" ? oci_core_subnet.workers_public[0].id : oci_core_subnet.workers_private[0].id
      }
    }
    size = lookup(var.node_pools[count.index], "size")
  }

  node_source_details {
    image_id    = lookup(var.node_pools[count.index], "image_id")
    source_type = "IMAGE"
  }

  node_shape     = lookup(var.node_pools[count.index], "node_shape")
  ssh_public_key = var.ssh_public_key
  depends_on     = [oci_containerengine_cluster.this]
  #lifecycle {
  #  ignore_changes = [size]
  #}
}
