module "oke-cluster-name" {
  source                     = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-oke.git?ref=master"
  compartment_id             = data.terraform_remote_state.oci-infra.outputs.cluster-name_compartment_id
  vcn_id                     = module.vcn-cluster-name.vcn_id
  availability_domains       = module.vcn-cluster-name.vcn_availability_domains
  kubernetes_version         = "v1.16.8"
  cluster_name               = "cluster-name"
  visibility_type            = "private"
  loadbalancer_mode          = "public"
  worker_private_subnet      = ["10.2.10.0/24"]
  loadbalancer_public_subnet = ["10.2.200.0/28"]
  worker_ssh_access_cidr     = ["10.2.0.0/16", "189.7.9.152/32"]
  allow_node_port_access     = true
  ssh_public_key             = "XXXXX"
  node_pools = [
    {
      name       = "nodes-infra"
      mode       = "private"
      node_shape = "VM.Standard2.2"
      size       = 2
      image_id   = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaqb5wvlvt3nikjyq3mjzvdnqq6zrcxsrp5iszr3d6f4lgimkdz32a"
      initial_node_labels = {
        node_pool = "nodes-infra"
      }
    },
    {
      name       = "nodes-stage"
      mode       = "private"
      node_shape = "VM.Standard2.1"
      size       = 1
      image_id   = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaqb5wvlvt3nikjyq3mjzvdnqq6zrcxsrp5iszr3d6f4lgimkdz32a"
      initial_node_labels = {
        node_pool = "nodes-stage"
      }
    }
  ]
}
