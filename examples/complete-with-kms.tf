module "iam_dynamic_group-cluster-name-oke-kms" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/dynamic-group?ref=master"
  compartment_id = var.tenancy_ocid
  description    = "Dynamic group to allow Oke Cluster to use KMS"
  name           = "cluster-name-oke-kms"
  matching_rule  = "ALL {resource.type = 'cluster', resource.compartment.id = '${module.iam_compartment-cluster-name.compartment_id}'}"
  #matching_rule = "ALL {resource.type = 'cluster', resource.id = '${module.oke-cluster-name.cluster_id}'}"
}

module "iam_policy_cluster-name-oke-kms" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-iam.git///modules/policy?ref=master"
  compartment_id = module.iam_compartment-cluster-name.compartment_id
  description    = "cluster-name-oke-kms"
  name           = "cluster-name-oke-kms"
  statements     = ["Allow dynamic-group ${module.iam_dynamic_group-cluster-name-oke-kms.dynamic_group_name} to use keys in compartment id ${module.iam_compartment-cluster-name.compartment_id} where target.key.id = '${module.kms_key-cluster-name-oke.kms_key_id}'"]
}

module "kms_vault-cluster-name" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-kms.git///modules/vault?ref=master"
  compartment_id = module.iam_compartment-cluster-name.compartment_id
  display_name   = "cluster-name"
  vault_type     = "DEFAULT"
}

module "kms_key-cluster-name-oke" {
  source              = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-kms.git///modules/key?ref=master"
  compartment_id      = module.iam_compartment-cluster-name.compartment_id
  display_name        = "cluster-name-oke"
  management_endpoint = module.kms_vault-cluster-name.kms_vault_management_endpoint
  key_shape = {
    algorithm = "AES"
    length    = 32
  }
}

module "oke-cluster-name" {
  source                     = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-oke.git?ref=master"
  compartment_id             = module.iam_compartment-cluster-name.compartment_id
  vcn_id                     = module.vcn-cluster-name.vcn_id
  availability_domains       = module.vcn-cluster-name.vcn_availability_domains
  kms_key_id                 = module.kms_key-cluster-name-oke.kms_key_id
  kubernetes_version         = "v1.16.8"
  cluster_name               = "cluster-name"
  visibility_type            = "private"
  loadbalancer_mode          = "public"
  worker_private_subnet      = ["10.2.10.0/24"]
  loadbalancer_public_subnet = ["10.2.200.0/28"]
  worker_ssh_access_cidr     = ["10.2.0.0/16", "189.7.9.152/32"]
  allow_node_port_access     = true
  ssh_public_key             = "XXXXX"
  node_pools = [
    {
      name       = "nodes-cluster-name"
      mode       = "private"
      node_shape = "VM.Standard2.2"
      size       = 2
      image_id   = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaqb5wvlvt3nikjyq3mjzvdnqq6zrcxsrp5iszr3d6f4lgimkdz32a"
      initial_node_labels = {
        node_pool = "nodes-cluster-name"
      }
    },
    {
      name       = "nodes-stage"
      mode       = "private"
      node_shape = "VM.Standard2.1"
      size       = 1
      image_id   = "ocid1.image.oc1.sa-saopaulo-1.aaaaaaaaqb5wvlvt3nikjyq3mjzvdnqq6zrcxsrp5iszr3d6f4lgimkdz32a"
      initial_node_labels = {
        node_pool = "nodes-stage"
      }
    }
  ]
}
