resource "oci_core_route_table" "public" {
  count          = var.visibility_type == "public" || var.visibility_type == "both" || var.loadbalancer_mode == "public" ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-public"
  freeform_tags  = var.freeform_tags

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = data.oci_core_internet_gateways.this.gateways[0].id
  }

  dynamic "route_rules" {
    for_each = var.worker_public_additional_route_table_rules
    content {
      destination       = lookup(route_rules.value, "destination")
      destination_type  = lookup(route_rules.value, "destination_type")
      network_entity_id = lookup(route_rules.value, "network_entity_id")
    }
  }

  vcn_id = var.vcn_id
}

resource "oci_core_route_table" "private" {
  count          = var.visibility_type == "private" || var.visibility_type == "both" || var.loadbalancer_mode == "private" ? 1 : 0
  compartment_id = var.compartment_id
  display_name   = "oke-${var.cluster_name}-private"
  freeform_tags  = var.freeform_tags
  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = data.oci_core_nat_gateways.this.nat_gateways[0].id
  }

  dynamic "route_rules" {
    for_each = var.visibility_type == "private" || var.visibility_type == "both" || var.loadbalancer_mode == "private" ? list(1) : []
    content {
      destination       = lookup(data.oci_core_services.this[0].services[0], "cidr_block")
      destination_type  = "SERVICE_CIDR_BLOCK"
      network_entity_id = data.oci_core_service_gateways.this.service_gateways[0].id
    }
  }

  dynamic "route_rules" {
    for_each = var.worker_private_additional_route_table_rules
    content {
      destination       = lookup(route_rules.value, "destination")
      destination_type  = lookup(route_rules.value, "destination_type")
      network_entity_id = lookup(route_rules.value, "network_entity_id")
    }
  }

  vcn_id = var.vcn_id
}
resource "oci_core_subnet" "workers_public" {
  count                      = var.visibility_type == "public" || var.visibility_type == "both" ? 1 : 0
  cidr_block                 = var.worker_public_subnet[count.index]
  compartment_id             = var.compartment_id
  display_name               = "oke-${var.cluster_name}-workers-public"
  dns_label                  = "workerspublic"
  prohibit_public_ip_on_vnic = false
  route_table_id             = oci_core_route_table.public[0].id
  security_list_ids          = [oci_core_security_list.workers_public[0].id]
  vcn_id                     = var.vcn_id
}

resource "oci_core_subnet" "workers_private" {
  count                      = var.visibility_type == "private" || var.visibility_type == "both" ? 1 : 0
  cidr_block                 = var.worker_private_subnet[count.index]
  compartment_id             = var.compartment_id
  display_name               = "oke-${var.cluster_name}-workers-private"
  dns_label                  = "workersprivate"
  prohibit_public_ip_on_vnic = true
  route_table_id             = oci_core_route_table.private[0].id
  security_list_ids          = [oci_core_security_list.workers_private[0].id]
  vcn_id                     = var.vcn_id
}

resource "oci_core_subnet" "loadbalancers_public" {
  count                      = var.loadbalancer_mode == "public" ? 1 : 0
  cidr_block                 = var.loadbalancer_subnet[count.index]
  compartment_id             = var.compartment_id
  display_name               = "oke-${var.cluster_name}-loadbalancers-public"
  dns_label                  = "lbpublic"
  prohibit_public_ip_on_vnic = false
  route_table_id             = oci_core_route_table.public[0].id
  security_list_ids          = [oci_core_security_list.loadbalancer_public[0].id]
  vcn_id                     = var.vcn_id
}

resource "oci_core_subnet" "loadbalancers_private" {
  count                      = var.loadbalancer_mode == "private" ? 1 : 0
  cidr_block                 = var.loadbalancer_subnet[count.index]
  compartment_id             = var.compartment_id
  display_name               = "oke-${var.cluster_name}-loadbalancers-private"
  dns_label                  = "lbprivate"
  prohibit_public_ip_on_vnic = true
  route_table_id             = oci_core_route_table.private[0].id
  security_list_ids          = [oci_core_security_list.loadbalancer_private[0].id]
  vcn_id                     = var.vcn_id
}

