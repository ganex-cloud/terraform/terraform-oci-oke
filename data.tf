data "oci_core_services" "this" {
  count = var.visibility_type == "private" || var.visibility_type == "both" ? 1 : 0
  filter {
    name   = "name"
    values = ["All .* Services In Oracle Services Network"]
    regex  = true
  }
}

data "oci_core_subnets" "oke_subnets" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id

  filter {
    name   = "state"
    values = ["AVAILABLE"]
  }
}

data "oci_core_internet_gateways" "this" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id
  filter {
    name   = "state"
    values = ["AVAILABLE"]
  }
}

data "oci_core_nat_gateways" "this" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id
  filter {
    name   = "state"
    values = ["AVAILABLE"]
  }
}

data "oci_core_service_gateways" "this" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id
  filter {
    name   = "state"
    values = ["AVAILABLE"]
  }
}

data "oci_core_vcn" "this" {
  vcn_id = var.vcn_id
}
